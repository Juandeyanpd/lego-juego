using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeMoney : MonoBehaviour
{
    public static TakeMoney Instance;
    public AudioClip[] sfx;
    public AudioSource aSource;

    public void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    public void PlaySoundSfx(Vector3 soundPoint, int id)
    {
        transform.position = soundPoint;
        aSource.PlayOneShot(sfx[id]);
    }



}
