using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public Animator anim;

    //[Range(0,1)]
    public float walkSpeed;
    public float walkSpeedMultiplier;
    private float speed;
    public float rotationSpeed;

    [Header("Jump")]
    public float jumpHeight;
    private float gravity = -9.8f;
    private Vector3 movementY;
    public bool isGround = false;
    public float sphereDistance;
    public float sphereRadius;
    public LayerMask lm;

    public Rigidbody rb;

    public bool run = false;

    public Checkpoint checkpoint;

    public int coin;

    private void Start()
    {
        speed = walkSpeed;
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        puch();
        Jump();
        MoveInXZ();
    }

    void Jump()
    {
        isGround = (Physics.SphereCast(transform.position, sphereRadius, Vector3.down, out RaycastHit hit, sphereDistance, lm));

        if (isGround && Input.GetButtonDown("Jump"))
        {
            anim.SetTrigger("Jump");
            TakeMoney.Instance.PlaySoundSfx(transform.position, 1);
            rb.AddForce(new Vector3(0, jumpHeight, 0), ForceMode.Impulse);
        }
    }

    void MoveInXZ()
    {
        
        Vector3 movementDirection = new Vector3(0, 0, Input.GetAxis("Vertical"));
        anim.SetFloat("Walk", movementDirection.z);

        transform.Rotate(new Vector3(0, Input.GetAxis("Horizontal") * rotationSpeed, 0));

        if (Input.GetButtonDown("Fire3"))
        {
            anim.SetTrigger("Speed");
            TakeMoney.Instance.PlaySoundSfx(transform.position, 2);
            speed = walkSpeed * walkSpeedMultiplier;
        }

        if(Input.GetButtonUp("Fire3"))
        {
            speed = walkSpeed;
            //TakeMoney.Instance.PlaySoundSfx(transform.position, );
        }

        transform.Translate((movementDirection * speed) * Time.deltaTime);
    }

    IEnumerator Dead()
    {
        yield return new WaitForSeconds(1f);
        anim.SetTrigger("Die");
        transform.position = checkpoint.spwanPoint;
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.transform.tag == "Lava")
        {
            transform.position = checkpoint.spwanPoint;
            StartCoroutine(Dead());
        }

        if(other.transform.tag == "Checkpoint")
        {
            checkpoint = other.GetComponent<Checkpoint>();
        }

        if(other.transform.tag == "Money")
        {
            TakeMoney.Instance.PlaySoundSfx(other.transform.position, 0);
            other.gameObject.SetActive(false);
            coin++;
        }

        if(other.transform.tag == "Win")
        {
            SceneManager.LoadScene(1);
        }
    }

    private void puch()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            anim.SetTrigger("Puch");
        }
    }
}
